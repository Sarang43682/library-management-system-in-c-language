#ifndef _LIBRARY_H
#define _LIBRARY_H

#define ROLE_OWNER "owner"
#define ROLE_LIBRARIAN "librarian"
#define ROLE_MEMBER "member"
#define STATUS_AVAIL "available"
#define STATUS_ISSUED "issued"
#define PAYMENT_FEES "fees"
#define PAYMENT_FINE "fine"
#define FINE_PER_DAY           5
#define BOOK_RETUEN_DAYS       7
#define MEMBERSHIP_MONTH_DAYS  30 

#include "date.h"



struct user
{
    int id;
    char name[10];
    char email[30];
    char phone[12];
    char password[10];
    char role[15];
};

struct book
{
    int id;
    char name[110];
    char author[50];
    char subject[50];
    double price;
    char isbn[25];
};

struct bookcopy
{
    int id;
    int bookid;
    int rack;
    char status[18];
};

struct issuerecord
{
    int id;
    int copyid;
    int memberid;
    date_t issue_date;
    date_t return_date;
    double fine_amount;
};

struct payment
{
    int id;
    int memberid;
    double amount;
    char type[10];
    date_t tx_time;
    date_t next_pay_duedate;
};

// owner function

// librarian function
// member function
// common function

#endif

